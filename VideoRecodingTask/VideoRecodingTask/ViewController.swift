//
//  ViewController.swift
//  VideoRecodingTask
//
//  Created by Apple on 29/07/21.
//

import UIKit
import AVKit
import MobileCoreServices

class ViewController: UIViewController{
    
    var videoAndImageReview = UIImagePickerController()
    let videoFileName = "/video.mp4"
    var filesize:Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
  
    override func viewWillAppear(_ animated: Bool) {
        if filesize == 0.0{
            showAlert(title: "Video Saved", message: "Video size less then 1 mb")
        }else{
            showAlert(title: "Video Saved", message: "Video size \(filesize ?? 0.0) mb")
        }
    }
    
    @IBAction func recordBtn(_ sender: UIButton){
        // 1 Check if project runs on a device with camera available
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            // 2 Present UIImagePickerController to take video
            videoAndImageReview.sourceType = .camera
            videoAndImageReview.mediaTypes = [kUTTypeMovie as String]
            videoAndImageReview.delegate = self
            videoAndImageReview.videoMaximumDuration = 30
            present(videoAndImageReview, animated: true, completion: nil)
        }
        else {
            print("Camera is not available")
        }
    }
    
    
 
    @IBAction func playBtn(_ sender: UIButton){
        // Display Photo Library
        videoAndImageReview.sourceType = UIImagePickerController.SourceType.photoLibrary
        videoAndImageReview.mediaTypes = [kUTTypeMovie as String]
        videoAndImageReview.delegate = self
        present(videoAndImageReview, animated: true, completion: nil)
    }

}



extension ViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // 1
        if let selectedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
            
            let data = NSData(contentsOf: selectedVideo as URL)!
            filesize = Double(data.length / 1000) / 1000
            // Save video to the main photo album
            let selectorToCall = #selector(videoSaved(_:didFinishSavingWithError:context:))
            // 2
            UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
            // Save the video to the app directory
            let videoData = try? Data(contentsOf: selectedVideo)
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
            try! videoData?.write(to: dataPath, options: [])
        }
        
        // 3
        picker.dismiss(animated: true)
    }

    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
           DispatchQueue.main.async(execute: { () -> Void in
           })
        }
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
          message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
      }
       
}

